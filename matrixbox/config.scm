(define-module (matrixbox config)
  #:use-module (matrixbox bootstrap)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader u-boot)
  #:use-module (gnu services)
  #:use-module (gnu services avahi)
  #:use-module (gnu services networking)
  #:use-module (gnu packages certs)
  #:use-module (gnu system)
  #:use-module (gnu system nss)
  #:export (matrixbox-os))

(define matrixbox-os
  (operating-system
   (inherit matrixbox-os/bootstrap)

   (bootloader (bootloader-configuration
                (bootloader u-boot-rock64-rk3328-bootloader)
                (targets '("/dev/mmcblk0"))))

   (services (append (list
                      ;; Connect to WPA/etc. WiFi networks
                      (service wpa-supplicant-service-type)
                      ;; Broadcast system hostname over mDNS
                      (service avahi-service-type))
                     %bootstrap-services))

   ;; HTTPS access
   (packages (append (list nss-certs) %base-packages))

   ;; Allow resolution of '.local' host names with mDNS.
   (name-service-switch %mdns-host-lookup-nss)))

matrixbox-os
