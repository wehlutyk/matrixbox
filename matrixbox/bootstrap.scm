;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Marius Bakke <marius@gnu.org>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (matrixbox bootstrap)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader u-boot)
  #:use-module (gnu image)
  #:use-module (gnu packages linux)
  #:use-module (gnu platforms arm)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu system)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system image)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-26)
  #:export (matrixbox-os/bootstrap
            %bootstrap-services
            rock64-image-type
            bootstrap-raw-image))

(define %spidev-udev-rule
  (udev-rule
   "90-spidev.rules"
   (string-append "SUBSYSTEM==\"spidev\", "
                  "GROUP==\"spi\", "
                  "MODE=\"0660\"")))

(define %bootstrap-services
  (append (list
           ;; Synchronise system time
           (service ntp-service-type)
           ;; Let us access spidev
           (udev-rules-service 'spidev %spidev-udev-rule
                               #:groups '("spi"))
           ;; Let us connect through ssh
           (service dhcp-client-service-type)
           (service openssh-service-type
                    (openssh-configuration
                     (password-authentication? #f)
                     (authorized-keys
                      `(("sl" ,(local-file "sl.pub")))))))
          %base-services))

(define matrixbox-os/bootstrap
  (operating-system
   (host-name "matrixbox")
   (timezone "Asia/Tokyo")
   (locale "en_US.utf8")

   (bootloader (bootloader-configuration
                (bootloader u-boot-rock64-rk3328-bootloader)
                (targets '("/dev/sda"))))
   (initrd-modules '())
   (kernel linux-libre-arm64-generic)

   (file-systems (cons (file-system
                        (device (file-system-label "mb-root"))
                        (mount-point "/")
                        (type "ext4"))
                       %base-file-systems))

   (users (cons* (user-account
                  (name "sl")
                  (comment "Sébastien Lerique")
                  (group "users")
                  (home-directory "/home/sl")
                  (supplementary-groups
                   '("wheel" "netdev" "audio" "video" "spi"))
                  ;; Lets us do root work from an ssh connection, before
                  ;; anything else has been set up
                  (password (crypt "initial password" "$6$abc")))
                 %base-user-accounts))

   (services %bootstrap-services)))

(define rock64-image-type
  (image-type
   (name 'rock64-raw)
   (constructor (cut image-with-os
                     (raw-with-offset-disk-image (expt 2 24))
                     <>))))

(define bootstrap-raw-image
  (image
   (inherit
    (os+platform->image matrixbox-os/bootstrap aarch64-linux
                        #:type rock64-image-type))
   (name 'bootstrap-raw-image)))

bootstrap-raw-image
